FROM python:3.12
COPY .pypirc ./
RUN pip install nyspowcoo[api]==0.5.1 --index-url https://gitlab.com/api/v4/projects/53427795/packages/pypi/simple